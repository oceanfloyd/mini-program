var pt = {
  'para': require('./ptsdk-conf.js'),
  'version': '1.0.0'
};

var _ = {};

pt._queue = [];
// 是否已经获取到系统信息
pt.getSystemInfoComplete = false;

var _isFirstTime = false; //判断是否首次启动


var ArrayProto = Array.prototype,
  FuncProto = Function.prototype,
  ObjProto = Object.prototype,
  slice = ArrayProto.slice,
  toString = ObjProto.toString,
  hasOwnProperty = ObjProto.hasOwnProperty,
  LIB_VERSION = '1.0.0',
  LIB_TYPE = 'wechat',
  Page_ID = '',
  Event_Paras = {},
  ptGetData,
  user_ID = '',
  SessionStartTime,
  SessionId,
  sendInterval,
  sendPackLimit,
  sessionKeepAlive,
  backgroundContinued,
  loadConfigPeriod,
  serverTime,
  eventBegin,
  eventBeginName,
  eventStop,
  keepAliveTimer,
  NetWork,
  Language,
  OsVersion,
  AppVersion,
  SdkVersion,
  Channel,
  _timeZone;



ArrayProto.indexOf = function(val) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == val) return i;
  }
  return -1;
};
ArrayProto.remove = function(val) {
  var index = this.indexOf(val);
  if (index > -1) {
    this.splice(index, 1);
  }
};

// 读取配置设备默认值
var SendingTimeInterval = 10 * 1000,
  BackgroundContinued = 30 * 1000,
  MaxPostDataLength = 50 * 1024,
  LoadConfigPeriod = 300 * 1000,
  SessionKeepAlive = 300,
  TimeOff; // 判断本地时间与服务器时间差值

pt.lib_version = LIB_VERSION;

var logger = typeof logger === 'object' ? logger : {};
logger.info = function() {
  if (typeof console === 'object' && console.log) {
    try {
      return console.log.apply(console, arguments);
    } catch (e) {
      console.log(arguments[0]);
    }
  }
};

(function() {
  var nativeBind = FuncProto.bind,
    nativeForEach = ArrayProto.forEach,
    nativeIndexOf = ArrayProto.indexOf,
    nativeIsArray = Array.isArray,
    breaker = {};

  var each = _.each = function(obj, iterator, context) {
    if (obj === null) {
      return false;
    }
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; i++) {
        if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
          return false;
        }
      }
    } else {
      for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          if (iterator.call(context, obj[key], key, obj) === breaker) {
            return false;
          }
        }
      }
    }
  };
  _.logger = logger;
  //
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      for (var prop in source) {
        if (source[prop] !== void 0) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

})();

_.isJSONString = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
_.isObject = function(obj) {
  return (toString.call(obj) === '[object Object]') && (obj !== null);
};
_.isEmptyObject = function(obj) {
  if (_.isObject(obj)) {
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) {
        return false;
      }
    }
    return true;
  }
  return false;
};
_.isString = function(obj) {
  return toString.call(obj) === '[object String]';
};
_.isDate = function(obj) {
  return toString.call(obj) === '[object Date]';
};
_.isBoolean = function(obj) {
  return toString.call(obj) === '[object Boolean]';
};
_.isNumber = function(obj) {
  return (toString.call(obj) === '[object Number]' && /[\d\.]+/.test(String(obj)));
};

_.getAllData = function(callback) {
  var _getNetWork = function() {
    wx.getNetworkType({
      'success': function(t) {
        NetWork = t['networkType'];
      },
      'complete': _getSystemInfo
    })
  };
  // 判断设备
  var _getSystemInfo = function() {
    wx.getSystemInfo({
      'success': function(t) {
        SdkVersion = String(LIB_VERSION);
        AppVersion = pt.para.appVersion;
        OsVersion = t.system.split(' ')[1];
        Language = t['language'];
        if (callback) {
          callback();
        }
      }
    });
  };
  _getNetWork();
};
_.getConfig = function(callback) {
  wx.request({
    url: 'https://appconfigloader.ptengine.cn/app/appConfig?appId=5d1rklk88q',
    method: 'get',
    header: {
      'content-type': 'application/json;charset=UTF-8;' // 默认值
    },
    fail: function() {
      console.log('获取配置错误！');
    },
    success: function(res) {
      var _data = JSON.parse(res.data.content);

      sendInterval = _data.sendInterval; // 获取配置每10s发包
      backgroundContinued = _data.backgroundContinued; // 获取超时
      serverTime = _data.serverTime;
      loadConfigPeriod = _data.loadConfigPeriod;
      sessionKeepAlive = _data.sessionKeepAlive;

      TimeOff = +new Date() - serverTime;

      if (callback) {
        callback();
      }

    }
  });
};
_.getTimeZone = function() {
  var _diffDate = new Date().getTimezoneOffset();
  if (_diffDate < 0) {
    _timeZone = 'GMT+' + (-_diffDate / 60);
  } else if (_diffDate < 0) {
    _timeZone = 'GMT-' + _diffDate / 60;
  } else {
    _timeZone = 'GMT+' + 0;
  }
};

_.info = {
  properties: {
    packType: LIB_TYPE,
    projectId: pt.para.projectId,
    appId: pt.para.appId,
    packData: []
  },
  getSystem: function() {
    var _e = this.properties,
      _this = this;
    // 判断网络    
    var _getNetWork = function() {
      wx.getNetworkType({
        'success': function(t) {

        },
        'complete': _getSystemInfo
      })
    };
    // 判断设备
    var _getSystemInfo = function() {
      wx.getSystemInfo({
        'success': function(t) {
          _e.deviceModel = t['model'];
          _e.resolution = t.windowWidth + 'x' + t.windowHeight;
          _e.os = t.system.split(' ')[0];
          _e.deviceBranding = t['brand'];
        },
        'complete': _this.setStatusComplete
      });
    };
    _getNetWork();
  },
  setStatusComplete: function() {
    pt.getSystemInfoComplete = true; // 设置为true
    if (pt._queue.length > 0) {
      _.each(pt._queue, function(cxt) {
        pt.prepareData.apply(pt, slice.call(cxt));
      });
      pt._queue = [];
    }
  }
};

pt._ = _;

// 处理存放所有属性
pt.prepareData = function(p, callback) {
  if (!pt.getSystemInfoComplete) {
    pt._queue.push(arguments);
    return false;
  }
  var data = {
    deviceId: this.store.getDistinctId(),
    androidId: this.store.getDistinctId(),
    packTime: +new Date() - TimeOff,
    packId: '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8),
    properties: {}
  };

  var _packData = _.info.properties.packData;
  _packData.push(p);
  data.properties = _.extend({}, _.info.properties);
  wx.setStorageSync('pt_temp_data', data);

  if (callback) {
    callback();
  }

};
/**
 * pt.store 底层相关方法
 */
pt.store = {
  _state: {},
  getUUID: function() {
    return '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
  },
  setStorage: function() {

  },
  getStorage: function() {
    return wx.getStorageSync('ptapp2018_wechat') || '';
  },
  getFirstId: function() {
    return this._state.first_id;
  },
  getDistinctId: function() {
    return this._state.deviceId;
  },
  set: function(name, value) {
    var _obj = {};
    if (typeof name === 'string') {
      _obj[name] = value;
    } else if (typeof name === 'object') {
      _obj = name;
    }
    this._state = this._state || {};
    for (var i in _obj) {
      this._state[i] = _obj[i];
    }
    this.save();
  },
  save: function() {
    wx.setStorageSync('ptapp2018_wechat', JSON.stringify(this._state));
  },
  toState: function(d) {
    var _sta = null;
    if (_.isJSONString(d)) {
      _sta = JSON.parse(d);
      console.log('_sta.deviceId' + _sta.deviceId);
      if (_sta.deviceId) {
        this._state = _sta;
        console.log('this._state:' + JSON.stringify(this._state));
      } else {
        this.set('deviceId', this.getUUID());
      }
    } else {
      this.set('deviceId', this.getUUID());
    }
  },
  init: function() {
    var _info = this.getStorage();
    console.log('_info:' + _info);
    if (_info) {
      this.toState(_info);
    } else {
      var _time = (new Date()),
        _visit_time = _time.getTime();
      _time.setHours(23);
      _time.setMinutes(59);
      _time.setSeconds(60);
      this.set({
        'deviceId': this.getUUID()
          // 'firstVisitTime': _visit_time,
          // 'firstVisitDayTime': _time.getTime()
      });
    }
  }
};

console.log('存储' + pt.store.getStorage());

if (pt.store.getStorage() === '') {
  _isFirstTime = true;
}

// pt.setProfile = function(p, c) {
//   pt.prepareData({
//     type: 'profile_set',
//     properties: p
//   }, c);
// };

// pt.setOnceProfile = function(p, c) {
//   pt.prepareData({
//     type: 'profile_set_once',
//     properties: p
//   }, c);
// };

// 自定义追踪事件--高级埋码
pt.tracking = function(e, p, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'advancedEvent',
      eventType: 'custom',
      timestamp: +new Date() - TimeOff,
      sessionStartTime: SessionStartTime,
      sessionId: SessionId,
      eventName: e,
      eventParas: p,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};


// 错误捕捉--
pt.trackError = function(code, info, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'errorInfo',
      timestamp: +new Date() - TimeOff,
      sessionStartTime: SessionStartTime,
      sessionId: SessionId,
      errorInfo: info,
      errorCode: code,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
}

// 心跳包事件
pt.sendKeepAlive = function(c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'advancedEvent',
      eventType: 'custom',
      timestamp: +new Date() - TimeOff,
      sessionStartTime: SessionStartTime,
      sessionId: SessionId,
      eventName: 'pt_session_keepalive',
      eventParas: [],
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};

// 事件停留时长--begin
pt.getBegin = function(n) {
  eventBegin = +new Date() - TimeOff;
  eventBeginName = n;
};

// 事件停留时长--stop
pt.getStop = function(e, p, c) {
  eventStop = +new Date() - TimeOff;
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'advancedEvent',
      eventType: 'custom',
      timestamp: +new Date() - TimeOff,
      sessionStartTime: SessionStartTime,
      sessionId: SessionId,
      eventName: e,
      eventParas: p,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone,
      pt_durations: String(Math.floor((eventStop - eventBegin) / 1000) % 60)
    }, c);
  });
};

// 全局默认事件
pt.track = function(e, p, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    console.log('时间SessionStartTime:' + SessionStartTime)
    pt.prepareData({
      category: 'pv',
      pageId: Page_ID,
      pageName: Page_ID,
      sessionId: SessionId,
      sessionStartTime: SessionStartTime,
      timestamp: +new Date() - TimeOff,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};

// 设置pageName事件
pt.setPage = function(n, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'pv',
      pageId: Page_ID,
      pageName: n,
      sessionId: SessionId,
      sessionStartTime: SessionStartTime,
      timestamp: +new Date() - TimeOff,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};


// 登录判断
pt.trackSignup = function(id, e, p, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'login',
      timestamp: +new Date() - TimeOff,
      sessionId: SessionId,
      sessionStartTime: SessionStartTime,
      uid: id,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
  // pt.store.set('deviceId', id);
};

// loadConfigPeriod 读取配置时间
// serverTime 读取服务器时间
// backgroundContinued 超过30s，重新启动
// sessionKeepAlive 每次有事件入库的时候，需要重新启动---
// androidId  

/**
 * 拉取配置，
 * 发包的时候对包进行校验大小，拆包；拆包直至符合大小策略；
 * 
 * 进入后台发送就发送所有的包，并且把keepAlive干掉；进入后台后又进入到前台超过30s，重新计算sessionStart；重新启动心跳包
 * 
 */

// 启动包
pt.startUp = function(c) {
  console.log('TimeOff:' + TimeOff);
  // SessionStartTime = new Date().getTime();
  SessionStartTime = +new Date() - TimeOff;
  console.log('SessionStartTime:' + SessionStartTime);
  SessionId = '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
  _.getTimeZone();
  pt._.getAllData(function() {
    console.log('哈哈newWork;' + NetWork);
    console.log(Language);
    console.log(OsVersion);
    console.log(AppVersion);
    console.log(SdkVersion);
    console.log(Channel);

    pt.prepareData({
      category: 'sessionStart',
      timestamp: SessionStartTime,
      sessionId: SessionId,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};

// 页面停留时长
pt.getPageTime = function(t, p, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'pageDuration',
      timestamp: +new Date() - TimeOff,
      sessionId: SessionId,
      pt_page_durations: t,
      pageId: p,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};


// 默认分享的时候用
pt.share = function(e, p, c) {
  _.getTimeZone();
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'advancedEvent',
      eventType: 'custom',
      timestamp: +new Date() - TimeOff,
      sessionStartTime: SessionStartTime,
      sessionId: SessionId,
      eventName: e,
      eventParas: p,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: _timeZone
    }, c);
  });
};

// 转发的时候获取群
pt.getGrop = function(e) {
  wx.getShareInfo({
    shareTicket: e.shareTickets[0],
    success: function(res) {
      console.log('encryptedData:' + res.encryptedData);
      console.log('iv:' + res.iv);
    },
    fail: function(res) { console.log(res) },
    complete: function(res) {
      console.log('成功转发:' + JSON.stringify(res));
    }
  });
}


pt.publicAttr = function() {


};

pt.login = function(id) {
  var _firstId = pt.store.getFirstId(),
    _distinctId = pt.store.getDistinctId();


  if (id !== _distinctId) {
    if (_firstId) {
      pt.trackSignup(id);
    } else {
      // pt.store.set('first_id', _distinctId);
      pt.trackSignup(id);
    }
  }
};


pt.init = function() {
  this._.info.getSystem();
  this.store.init();
  // console.log('s参数:' + JSON.stringify(this.para));
  // if (_.isObject(this.para.publicAttr)) {
  //   // _.info.properties = _.extend(_.info.properties, this.para.publicAttr);
  //   // _.info.eventParas = _.extend(_.info.eventParas, this.para.publicAttr);
  //   console.log(this.para.publicAttr);
  // }

};

pt.send = function(t) {
  var o = 0;
  var url = '';

  // logger.info(t.properties);
  _.extend(t, t.properties);

  delete t.properties;

  logger.info(t);
  console.log('长度：' + (encodeURIComponent(JSON.stringify(t)).length / 1024));

  if ((encodeURIComponent(JSON.stringify(t)).length / 1024) > 50) {
    console.log('分包执行');
    var _paraPack = {};
    _.each(t, function(a, i) {
      if (i !== 'packData') {
        _paraPack[i] = a;
      }
    });
    var _paraPackLen = encodeURIComponent(JSON.stringify(_paraPack)).length / 1024;
    // var newArr = [];
    _.each(t, function(a, i) {
      if (i === 'packData') {
        var myArr = a;
        var _objs = [];

        function recursionArr(arr) {　　
          // 临时存放数组
          var _arr = [];
          // 对其进行分组
          var _num = Math.ceil(arr.length / 2);
          for (var i = 0; i < arr.length; i += _num) {
            _arr.push(arr.slice(i, i + _num));
          }
          var _pack = {},
            _obj;
          for (var i = 0; i < _arr.length; i++) {
            _obj = {};
            var _pack = {
              packData: _arr[i]
            };
            if (Number(_paraPackLen) + Number(encodeURIComponent(JSON.stringify(_pack)).length / 1024) > 50) {
              recursionArr(_arr[i]);
            } else {
              _.each(t, function(a, iu) {
                if (iu === 'packData') {
                  _obj['packData'] = _arr[i];
                } else {
                  _obj[iu] = a;
                }
              });
              _objs.push(_obj);
            }
          }
        }
        recursionArr(myArr);
        wx.setStorageSync('pt_temp_data1', _objs);
        var _ptTempData1 = wx.getStorageSync('pt_temp_data1');
        console.log('_ptTempData1.length:' + _ptTempData1.length);
        for (var i = 0; i < wx.getStorageSync('pt_temp_data1').length; i++) {
          t = JSON.stringify(_ptTempData1[i]);
          console.log(t);
          var sendRequest = function() {
            wx.request({
              url: pt.para.serverUrl,
              method: 'POST',
              header: {
                'content-type': 'application/json;charset=UTF-8;' // 默认值
              },
              data: encodeURIComponent(t),
              fail: function() {
                console.log('发送错误，重新发送！');
                o < 2 && (o++, sendRequest());
              },
              success: function(res) {
                _ptTempData1.remove(_ptTempData1[i]);
                wx.setStorageSync('pt_temp_data1', _ptTempData1);
                i = 0;
                // console.log(wx.getStorageSync('pt_temp_data1').length);
                if (wx.getStorageSync('pt_temp_data1').length === 1) {
                  wx.removeStorageSync('pt_temp_data1');
                }
                console.log('1分包数据采集发送完毕！');
              }
            });
          };
          sendRequest();
        }
      }
    });

  } else {
    t = JSON.stringify(t);
    console.log(t);

    var sendRequest = function() {
      wx.request({
        url: pt.para.serverUrl,
        method: 'POST',
        header: {
          'content-type': 'application/json;charset=UTF-8;' // 默认值
        },
        data: encodeURIComponent(t),
        fail: function() {
          console.log('发送错误，重新发送！');
          o < 2 && (o++, sendRequest());
        },
        success: function(res) {
          console.log('正常数据采集发送完毕！');
        }
      });
    };
    sendRequest();
  }
};

function e(t, n, o) {
  if (t[n]) {
    var e = t[n];
    t[n] = function(t) {
      return o.call(this, t, n), e.call(this, t)
    }
  } else
    t[n] = function(t) {
      return o.call(this, t, n)
    }
}

function appLaunch(options) {



  wx.getLocation({
    type: 'wgs84',
    success: function(res) {
      var latitude = res.latitude;
      var longitude = res.longitude;
      var speed = res.speed;
      var accuracy = res.accuracy;

      console.log('latitude:' + latitude);
      console.log('longitude:' + longitude);
      console.log('speed:' + speed);
      console.log('accuracy:' + accuracy);
    }
  })


  wx.getUserInfo({
    success: function(res) {
      var userInfo = res.userInfo;
      var nickName = userInfo.nickName;
      var avatarUrl = userInfo.avatarUrl;
      var gender = userInfo.gender; //性别 0：未知、1：男、2：女
      var province = userInfo.province;
      var city = userInfo.city;
      var country = userInfo.country;
      console.log('userInfo:' + JSON.stringify(userInfo));
    }
  });



  wx.removeStorageSync('houtai');

  // wx.request({
  //   url: 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxab41c3c63b870056&secret=a646be2157c33812a40bae51506d20b0',
  //   method: 'get',
  //   header: {
  //     'content-type': 'application/json;charset=UTF-8;' // 默认值
  //   },
  //   success: function(res) {
  //     console.log('acton:' + res);
  //   }
  // });

  // 每隔loadConfigPeriod重新刷新下配置
  setInterval(function() {
    pt._.getConfig();
  }, loadConfigPeriod || LoadConfigPeriod);


  var _this = this;
  pt._.getConfig(function() {

    pt.startUp(function() {
      if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
        wx.getNetworkType({
          success: function(res) {
            var _network = res.networkType;
            // 无网络先把包存起来
            if (_network === 'none') {
              // wx.setStorageSync('pt_temp_data', data);
            } else {
              // 有网络
              // 先判断有没有存储
              pt.send(wx.getStorageSync('pt_temp_data'));
              _.info.properties.packData = [];
              wx.removeStorageSync('pt_temp_data');
              console.log('先发一次sessionStart');
            }
          }
        });
      }
    });

    _this[pt.para.proName] = pt;
    pt.init();
    //   pt.prepareDate(2, 5);

    // 获取场景值
    // _.info.properties.channel = String(options.scene);
    Channel = String(options.scene);

    console.log('公众号：' + JSON.stringify(options));

    // console.log('值：' + wx.getStorageSync('pt_temp_data'));

    var _a = 1;
    setInterval(function() {
      // console.log(sendInterval);
      _a++;
      if (_a === (sendInterval || SendingTimeInterval) / 100) { // 10s发包
        _a = 1;
        // console.log('长度：' + wx.getStorageSync('pt_temp_data').properties.packData.length);
        if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
          wx.getNetworkType({
            success: function(res) {
              var _network = res.networkType;
              // 无网络先把包存起来
              if (_network === 'none') {
                // wx.setStorageSync('pt_temp_data', data);
              } else {
                // 有网络
                // 先判断有没有存储
                pt.send(wx.getStorageSync('pt_temp_data'));
                _.info.properties.packData = [];
                wx.removeStorageSync('pt_temp_data');
                console.log('10s一发');
              }
            }
          });
        }
      }
      // console.log(_a);
    }, 100);
  });

};


function appShow(ops) {

  // var _timer = setInterval(function() {
  //   if (TimeOff !== null) {
  //     pt.startUp();
  //     clearInterval(_timer);
  //   }
  // }, 500);

  console.log('onShow参数：' + JSON.stringify(ops));

  var _shareTicket;
  if (ops.scene === 1044) {
    _shareTicket = ops.shareTicket;

    wx.getShareInfo({
      shareTicket: _shareTicket,
      success: function(res) {
        console.log('sdk数据：encryptedData:' + res.encryptedData);
        console.log('iv:' + res.iv);
      },
      complete: function(res) {
        console.log('成功转发:' + JSON.stringify(res));
      }
    });

  }


  /**
   * @keepAliveTimer 心跳包计时器
   */
  var _num = 1;
  keepAliveTimer = setInterval(function() {
    _num++;
    // console.log('_num:' + _num);
    if (_num === (sessionKeepAlive || SessionKeepAlive)) {
      pt.sendKeepAlive();
      _num = 1;
    }
  }, 1000);

  console.log('存储：' + wx.getStorageSync('houtai'));
  if (wx.getStorageSync('houtai') === 10) {
    if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
      wx.getNetworkType({
        success: function(res) {
          var _network = res.networkType;
          // 无网络先把包存起来
          if (_network === 'none') {
            // wx.setStorageSync('pt_temp_data', data);
          } else {
            // 有网络
            // 先判断有没有存储
            pt.send(wx.getStorageSync('pt_temp_data'));
            _.info.properties.packData = [];
            wx.removeStorageSync('pt_temp_data');
            pt.startUp();
          }
        }
      });
    } else {
      pt.startUp();
    }

    console.log('后台时间超过10s');

  } else {
    console.log('不够10s：' + wx.getStorageSync('houtai'));
  }


};

function appHide(n, e) {

  // 切换到后台的时候，清空心跳包
  clearInterval(keepAliveTimer);

  pt.tracking('closeMiNi');

  // wx.setStorageSync('houtai');

  // var wait = 10;
  // var time = function() {
  //   if (wait === 0) {
  //     wait = 0;

  //     // pt.send(wx.getStorageSync('cunshu'), wx.removeStorageSync('cunshu'));

  //   } else {
  //     wait--;
  //     console.log(wait);
  //     wx.setStorageSync('houtai', wait);
  //     setTimeout(function() {
  //       time();
  //     }, 1000);
  //   }
  // };
  // time();

  // 小程序退到后台后，先发送所有的数据
  if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
    wx.getNetworkType({
      success: function(res) {
        var _network = res.networkType;
        // 无网络先把包存起来
        if (_network === 'none') {
          // wx.setStorageSync('pt_temp_data', data);
        } else {
          // 有网络
          // 先判断有没有存储
          pt.send(wx.getStorageSync('pt_temp_data'));
          _.info.properties.packData = [];
          wx.removeStorageSync('pt_temp_data');
        }
      }
    });
  }


  var _a = 1;
  var _timer = setInterval(function() {
    // console.log(sendInterval);
    _a++;
    wx.setStorageSync('houtai', _a);
    if (_a === 10) { // 10s发包
      _a = 1;
      clearInterval(_timer);
      // wx.removeStorageSync('houtai');
    }
    // console.log(_a);
  }, 1000);
};


var p = App;

App = function(t) {
  e(t, "onLaunch", appLaunch);
  e(t, "onShow", appShow);
  e(t, "onHide", appHide);
  p(t);
};

function pageOnunload(n, e) {

}

function pageOnload(t, n) {

  // console.log(11343534);

  wx.showShareMenu({
    withShareTicket: true
  });


};

function pageShare(t, n) {
  var router = typeof this['__route__'] === 'string' ? this['__route__'] : '系统没有取到值';

  // 默认采集转发事件以及获取转发的路径,若值为false，并需要采集转发信息，需要手动埋点
  if (pt.para.getShare) {
    pt.share('share', { path: router });
  }

  // if (t.from === 'button') {
  //   // 来自页面内转发按钮
  //   console.log(t.target)
  // }

  console.log('n:' + n);


  // wx.getShareInfo({
  //   shareTicket: _shareTicket,
  //   success: function(res) {
  //     console.log('sdk数据：encryptedData:' + res.encryptedData);
  //     console.log('iv:' + res.iv);
  //   },
  //   complete: function(res) {
  //     console.log('成功转发:' + JSON.stringify(res));
  //   }
  // });

  return {
    title: 'age',
    path: router,
    success: function(t) {

      // console.log(1111);

      console.log('sdk转发：' + res.shareTickets[0]);


    },
    fail: function(res) {
      // 转发失败
      console.log(res);
    }
  }


};


function pageOnshow(t, n) {
  var router = typeof this['__route__'] === 'string' ? this['__route__'] : '系统没有取到值';

  Page_ID = router;
  if (pt.para.onshow) {
    pt.para.onshow(pt, router, this);
    // pt.setPage1();
  } else {

    // console.log('t:' + '自己的方法');

    // if (pt.setPage) {
    //   console.log('调用了');
    // } else {
    //   console.log('没有调用');
    // }

    // pt.setPage1();

    pt.track('pv', {
      // pageId: router
    });

    // 页面停留时长
    if (wx.getStorageSync('pt_page_time') !== '') {
      if (wx.getStorageSync('pt_page_time').ptPage !== Page_ID) {
        var _leveTime = new Date().getTime() - wx.getStorageSync('pt_page_time').ptTime,
          _page = wx.getStorageSync('pt_page_time').ptPage;

        pt.getPageTime(Math.floor(_leveTime / 1000) % 60, _page);

        var _ptTimePage = {
          ptTime: new Date().getTime(),
          ptPage: Page_ID
        };
        wx.setStorageSync('pt_page_time', _ptTimePage);
      };
    } else {
      var _ptTimePage = {
        ptTime: new Date().getTime(),
        ptPage: Page_ID
      };
      wx.setStorageSync('pt_page_time', _ptTimePage);
    }
  }


};

var v = Page;
Page = function(t) {
  e(t, 'onLoad', pageOnload);
  e(t, 'onUnload', pageOnunload);
  e(t, 'onShow', pageOnshow);
  e(t, 'onHide', pageOnunload);
  e(t, 'onShareAppMessage', pageShare);
  v(t);
}


module.exports = pt;