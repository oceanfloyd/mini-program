var pt = {
  'para': require('./ptsdk-conf.js'),
  'version': '1.0.0'
};

var _ = {};

pt._queue = [];
// 是否已经获取到系统信息
pt.getSystemInfoComplete = false;

var ArrayProto = Array.prototype,
  FuncProto = Function.prototype,
  ObjProto = Object.prototype,
  slice = ArrayProto.slice,
  toString = ObjProto.toString,
  hasOwnProperty = ObjProto.hasOwnProperty,
  LIB_VERSION = '1.0.0',
  LIB_NAME = 'PtProgram';

pt.lib_version = LIB_VERSION;

var logger = typeof logger === 'object' ? logger : {};
logger.info = function() {
  if (typeof console === 'object' && console.log) {
    try {
      return console.log.apply(console, arguments);
    } catch (e) {
      console.log(arguments[0]);
    }
  }
};

(function() {
  var nativeBind = FuncProto.bind,
    nativeForEach = ArrayProto.forEach,
    nativeIndexOf = ArrayProto.indexOf,
    nativeIsArray = Array.isArray,
    breaker = {};

  var each = _.each = function(obj, iterator, context) {
    if (obj === null) {
      return false;
    }
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; i++) {
        if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
          return false;
        }
      }
    } else {
      for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          if (iterator.call(context, obj[key], key, obj) === breaker) {
            return false;
          }
        }
      }
    }
  };
  _.logger = logger;
  //
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      for (var prop in source) {
        if (source[prop] !== void 0) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

})();

_.isJSONString = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
_.isObject = function(obj) {
  return (toString.call(obj) === '[object Object]') && (obj !== null);
};
_.isEmptyObject = function(obj) {
  if (_.isObject(obj)) {
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) {
        return false;
      }
    }
    return true;
  }
  return false;
};
_.isString = function(obj) {
  return toString.call(obj) === '[object String]';
};
_.isDate = function(obj) {
  return toString.call(obj) === '[object Date]';
};
_.isBoolean = function(obj) {
  return toString.call(obj) === '[object Boolean]';
};
_.isNumber = function(obj) {
  return (toString.call(obj) === '[object Number]' && /[\d\.]+/.test(String(obj)));
};

_.info = {
  properties: {
    // $lib: LIB_NAME,
    // sdkVersion: String(LIB_VERSION),
    // $user_agent: 'PT MP SDK'
  },
  getSystem: function() {
    var _e = this.properties,
      _this = this;
    // 判断网络    
    var _getNetWork = function() {
      wx.getNetworkType({
        'success': function(t) {
          _e.network = t['networkType'];
          console.log('网络：' + _e.network);
        },
        'complete': _getSystemInfo
      })
    };
    // 判断设备
    var _getSystemInfo = function() {
      wx.getSystemInfo({
        'success': function(t) {
          _e.deviceBranding = t['model'];
          // _e.$screen_w = Number(t['windowWidth']);
          // _e.$screen_h = Number(t['windowHeight']);
          _e.resolution = {
            screen_w: Number(t['windowWidth']),
            screen_h: Number(t['windowHeight'])
          };
          _e.os = t.system.split(' ')[0];
          _e.deviceModel = t.system.split(' ')[1];
        },
        'complete': _this.setStatusComplete
      });
    };
    _getNetWork();
  },
  setStatusComplete: function() {
    pt.getSystemInfoComplete = true; // 设置为true
    if (pt._queue.length > 0) {
      _.each(pt._queue, function(cxt) {
        pt.prepareData.apply(pt, slice.call(cxt));
      });
      pt._queue = [];
    }
  }
};

pt._ = _;


// 处理存放所有属性
pt.prepareData = function(p, callback) {
  if (!pt.getSystemInfoComplete) {
    pt._queue.push(arguments);
    return false;
  }
  var data = {
    UUID: this.store.getDistinctId(),
    // lib: {
    // $lib: LIB_NAME,
    // $lib_method: 'code',
    // sdkVersion: String(LIB_VERSION)
    // },
    sdkVersion: String(LIB_VERSION),
    properties: {}
  };
  _.extend(data, p);

  // 合并properties里的属性
  if (_.isObject(p.properties) && !_.isEmptyObject(p.properties)) {
    _.extend(data.properties, p.properties);
  }

  // profile时不传公共属性
  if (!p.type || p.type.slice(0, 7) !== 'profile') {
    // 传入的属性 > 当前页面的属性 > session的属性 > cookie的属性 >预定义属性
    // console.log('type类型:' + p.type);
    data.properties = _.extend({}, _.info.properties, data.properties);
  }

  // 如果$time是传入的就用，否则使用服务器端时间
  if (data.properties.$time && _.isDate(data.properties.$time)) {
    data.time = data.properties.$time * 1;
    delete data.properties.$time;
  } else {
    if (pt.para.isClientTime) {
      data.time = (new Date()) * 1;
    }
  }


  // 判断是否首日访问
  if (typeof pt.store._state === 'object' && typeof pt.store._state.first_visit_day_time === 'number' && pt.store._state.first_visit_day_time > (new Date().getTime())) {
    data.properties.firstDay = true;
  } else {
    data.properties.firstDay = false;
  }

  pt.send(data, callback);

};
/**
 * pt.store 底层相关方法
 */
pt.store = {
  _state: {},
  getUUID: function() {
    return '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
    // return pt.para.weTrackId;
  },
  setStorage: function() {

  },
  getStorage: function() {
    return wx.getStorageSync('ptapp2018_wechat') || '';
  },
  getFirstId: function() {
    return this._state.first_id;
  },
  getDistinctId: function() {
    return this._state.UUID;
  },
  set: function(name, value) {
    var _obj = {};
    if (typeof name === 'string') {
      _obj[name] = value;
    } else if (typeof name === 'object') {
      _obj = name;
    }
    this._state = this._state || {};
    for (var i in _obj) {
      this._state[i] = _obj[i];
    }
    this.save();
  },
  save: function() {
    wx.setStorageSync('ptapp2018_wechat', JSON.stringify(this._state));
  },
  toState: function(d) {
    var _sta = null;
    if (_.isJSONString(d)) {
      _sta = JSON.parse(d);
      if (_sta.UUID) {
        this._state = _sta;
      } else {
        this.set('UUID', this.getUUID());
      }
    } else {
      this.set('UUID', this.getUUID());
    }
  },
  init: function() {
    var _info = this.getStorage();
    if (_info) {
      this.toState(_info);
    } else {
      var _time = (new Date()),
        _visit_time = _time.getTime();
      _time.setHours(23);
      _time.setMinutes(59);
      _time.setSeconds(60);
      this.set({
        'UUID': this.getUUID(),
        'first_visit_time': _visit_time,
        'first_visit_day_time': _time.getTime()
      });
    }
  }
};

pt.setProfile = function(p, c) {
  pt.prepareData({
    type: 'profile_set',
    properties: p
  }, c);
};

pt.setOnceProfile = function(p, c) {
  pt.prepareData({
    type: 'profile_set_once',
    properties: p
  }, c);
};

pt.track = function(e, p, c) {
  this.prepareData({
    type: 'track',
    event: e,
    properties: p
  }, c);
};

pt.trackSignup = function(id, e, p, c) {
  pt.prepareData({
    original_id: pt.store.getFirstId() || pt.store.getDistinctId(),
    UUID: id,
    type: 'track_signup',
    event: e,
    properties: p
  }, c);
  pt.store.set('UUID', id);
};

// 分享的时候用
pt.share = function(p, c) {
  pt.prepareData({
    type: 'share',
    shareConts: p
  }, c);
}

pt.publicAttr = function() {


};

pt.login = function(id) {
  var _firstId = pt.store.getFirstId(),
    _distinctId = pt.store.getDistinctId();
  if (id !== _distinctId) {
    if (_firstId) {
      pt.trackSignup(id, '$SignUp');
    } else {
      pt.store.set('first_id', _distinctId);
      pt.trackSignup(id, '$SignUp');
    }
  }
};


pt.init = function() {
  this._.info.getSystem();
  this.store.init();
  // console.log('s参数:' + JSON.stringify(this.para));
  if (_.isObject(this.para.publicAttr)) {
    _.info.properties = _.extend(_.info.properties, this.para.publicAttr);
  }
};

pt.send = function(t) {
  var o = 0;
  var url = '';
  // t._nocache = (String(Math.random()) + String(Math.random()) + String(Math.random())).slice(2, 15);

  logger.info(t);
  t = JSON.stringify(t);
  // if (pt.para.serverUrl.indexOf('?') !== -1) {
  //   url = pt.para.serverUrl + '&data=' + t;
  // } else {
  //   url = pt.para.serverUrl + '?data=' + t;
  // }

  // console.log('url:' + url);
  console.log('tt:' + t);
  console.log(encodeURIComponent(t));

  var sendRequest = function() {
    wx.request({
      url: pt.para.serverUrl,
      method: 'POST',
      header: {
        'content-type': 'application/json;charset=UTF-8;' // 默认值
      },
      data: encodeURIComponent(t),
      fail: function() {
        console.log('发送错误，重新发送！');
        o < 2 && (o++, sendRequest());
      },
      success: function(res) {
        console.log('数据采集发送完毕！');
      }
    });
  };
  sendRequest();
};

function e(t, n, o) {
  if (t[n]) {
    var e = t[n];
    t[n] = function(t) {
      o.call(this, t, n), e.call(this, t)
    }
  } else
    t[n] = function(t) {
      o.call(this, t, n)
    }
}

function appLaunch() {
  this[pt.para.proName] = pt;
  pt.init();
  //   pt.prepareDate(2, 5);
};

function appShow() {

};

function appHide(n, e) {

};



var p = App;

App = function(t) {
  e(t, "onLaunch", appLaunch);
  e(t, "onShow", appShow);
  e(t, "onHide", appHide);
  p(t);
};

function pageOnunload(n, e) {

}

function pageOnload(t, n) {

};



function pageOnshow(t, n) {
  var router = typeof this['__route__'] === 'string' ? this['__route__'] : '系统没有取到值';

  console.log('当前页面router:' + router);

  if (pt.para.onshow) {
    pt.para.onshow(pt, router, this);
  } else {
    pt.track('DefaultEvent', {
      pageId: router
    });
  }
};

var v = Page;
Page = function(t) {
  e(t, 'onLoad', pageOnload);
  e(t, 'onUnload', pageOnunload);
  e(t, 'onShow', pageOnshow);
  e(t, 'onHide', pageOnunload);
  v(t);
}


module.exports = pt;