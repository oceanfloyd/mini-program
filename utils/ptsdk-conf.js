var conf = {
  proName: 'pt',
  projectId: 'eoybkf7gqjeu', // 项目id
  appId: 'o1ivkpn7iw', // 微信小程序id
  serverUrl: 'https://tsappcollectservice.ptengine.cn/app',
  // 正式环境
  // projectId:'n6fq0znxeqhh',
  // appId:'5d1rklk88q',
  // serverUrl: 'https://appcollectservice.ptengine.cn/app',
  // weTrackId: 'ptxxxId&vetege90',
  // https://appcollectservice.ptengine.cn/app
  // https://tsappcollectservice.ptengine.cn/app


  // 小程序版本号
  appVersion: '1.0.1',
  // 默认采集转发事件以及获取转发的路径,若值为false，并需要采集转发信息，需要手动埋点
  getShare: false,
  // 定义一些公共属性
  publicAttr: {
    weChatName: 'su购',
    time: '2018/01/01'
  },
  // 如果需要向 pageOnshow 方法中添加自定义参数（ pt.track 方法中添加参数)则保留 onshow 方法，否则删除下面方法即可。
  // onshow: function(pt, router) {
  //   pt.track('pv', {
  //     // pageId: router
  //   });
  // }
};

module.exports = conf;