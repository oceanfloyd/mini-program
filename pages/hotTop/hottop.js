var app = getApp()
Page({
  data: {
    tips: []
  },
  bindEvent1: function() {
    // app.tdsdk.event({
    //   id: 'eventId',
    //   label: 'eventLabel',
    //   params: {
    //     key1: '什么值',
    //     key2: 'value2'
    //   }
    // });
    app.pt.tracking('setImg', { name: '刘德华', images: '图片' });

    // 模拟转发
    // app.pt.share({ title: '自定义转发标题', path: '/page/user?id=123' });
  },
  bindEvent2: function() {
    app.pt.tracking('setColor', { color: 'red' });
  },
  bindEvent3: function() {
    app.pt.tracking('getIt', { it: 'java' });
  },
  bindEvent4: function() {
    app.pt.tracking('getStars', { city: '香港', name: '张学友' });
  },
  bindEvent5: function() {
    app.pt.tracking('getZhengXie', { name: '雷军' });
  },
  bindEvent6: function() {
    app.pt.tracking('setStarMoves', { name: '红海行动' });
  }
})