//index.js
//获取应用实例
// var pt = require('../../utils/sdk.js');
// var sensors = require('../../utils/sensorsdata.js');
var app = getApp()

Page({
  data: {
    images: {}
  },
  imageLoad: function(e) {
    var $width = e.detail.width, //获取图片真实宽度
      $height = e.detail.height,
      ratio = $width / $height; //图片的真实宽高比例
    var viewWidth = 718, //设置图片显示宽度，左右留有16rpx边距
      viewHeight = 718 / ratio; //计算的高度值
    var image = this.data.images;
    //将图片的datadata-index作为image对象的key,然后存储图片的宽高值
    image[e.target.dataset.index] = {
      width: viewWidth,
      height: viewHeight
    }
    this.setData({
      images: image
    })
  },
  onLoad: function() {
    console.log('点点滴滴');
  },
  bindEvent: function() {
    // app.tdsdk.event({
    //   id: 'eventId',
    //   label: 'eventLabel',
    //   params: {
    //     key1: '什么值',
    //     key2: 'value2'
    //   }
    // });
    // app.pt.tracking('clickImage', { name: '桃花', images: '图文', genders: { man: '南', women: '女' } });
    // app.sensors.track('clickImage', { name: '桃花', images: '图文' });

    app.pt.login('1111');

    // 模拟转发
    // app.pt.share({ title: '自定义转发标题', path: '/page/user?id=123' });
  },
  onReady: function() {
    // app.onready();
    // var query = wx.createSelectorQuery()
    // query.selectAll('.conts-li').boundingClientRect()
    // query.selectViewport().scrollOffset()
    // query.exec(function(res) {
    //   console.log(res);
    //   res[0].top // #the-id节点的上边界坐标
    //   res[1].scrollTop // 显示区域的竖直滚动位置
    // })
    // console.log(app);
    // console.log(app.con('1'));
    this.parseHtml('page');
  },
  parseHtml(htmlBlock) {
    var parser = new DOMParser();
    return parser.parseFromString(htmlBlock, "text/html");
  },
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log('哈哈res.target:' + res.target);
    }
    console.log('页面的');
    return {
      title: 'ptmind大数据时代',
      // path: 'pages/index/index',
      success: function(res) {
        // 转发成功
        app.pt.share({ title: 'ptmind大数据时代', path: '/pages/index/index' });
      },
      fail: function(res) {
        // 转发失败
      }
    }
  }
})